import csv
from .models import Drainage

if 1:
    if 1:
        if 1:
            if 1:
                with open('C:/Users/HP/Desktop/QGIS Plugin Test/New Test Dataset/example_drainage_input.csv', 'r', newline='') as f:
                    drainage_data = list(csv.DictReader(f))

                connected_streams = {}
                for i in drainage_data:
                    connected_streams[i['stream_id']] = Drainage.ConnectedStream(
                        Drainage.Stream.Channel(*[float(i[v]) for v in [
                            'watershed_area', 'length', 'width_bottom', 'channel_slope', 'fraction_deep_aquifer', 'zch',
                            'hydraulic_conductivity', 'evaporation_coefficient', 'mannigs', 'bank_flow_recession', 'potential_evaporation'
                        ]]),
                        [Drainage.Stream.Transient(volume_out=0, volume_stored_end_timestep=0)]
                    )
                    connected_streams[i['stream_id']].stream_id = i['stream_id']
               

                #get runoff data from input file
                i = 1
                while str(i) in drainage_data[0].keys():
                    i += 1
                total_time_steps = i - 1




                
                for i in drainage_data:
                    connected_streams[i['stream_id']].sources.extend([
                        connected_streams[v.strip()] for v in i['sources'].split(',') if v.strip() != ''
                    ])
                    
                    connected_streams[i['stream_id']].runoffs = [float(i[str(j+1)]) for j in range(total_time_steps)]
                
                





                drainage = Drainage(list(connected_streams.values()))

                for i in range(total_time_steps):
    
                    for cs in drainage.connected_streams:
                        cs.next_runoff_per_area_in_watershed = cs.runoffs[i]

                    drainage.compute_drainage_model_transients_for_latest_time_step()


                with open('C:/Users/HP/Desktop/QGIS Plugin Test/New Test Dataset/my_output.csv', 'w', newline='') as f:
                    fieldnames = [
                        'runoff_per_area_in_watershed', 'swat_runoff', 'total_volume_stored', 'volume_in', 'discharge',
                        'transmission_loss', 'bankin', 'return_flow_from_bank', 'evaporation_loss', 'total_loss',
                        'volume_after_loss', 'volume_out', 'volume_stored_end_timestep', 'cross_section',
                        'depth_water_level', 'width_water_level', 'wetted_perimeter', 'hydraulic_radius',
                        'velocity', 'travel_time', 'fraction_time_step', 'storage_coeffecient'
                        ]
                    writer = csv.writer(f)
                    writer.writerow(['stream_id', 'time_step'] + fieldnames)
                    for cs in drainage.connected_streams:
                        #print(cs.stream_id,': Done')
                        for i in range(total_time_steps):
                            writer.writerow([cs.stream_id, str(i)] + [getattr(cs.transients[i], fn) for fn in fieldnames])
